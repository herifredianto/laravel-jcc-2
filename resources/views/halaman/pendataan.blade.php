@extends('layout.master')
@section('judul')
    <h1>Buat Account Baru</h1> <br>
@endsection
   @section('content')
        
    <h2>Sign Up Form</h2> <br>
    <form action="/kirim" method="post">
        @csrf
        <label for="">Nama Lengkap</label> <br>
        <input type="text" name="nama"> <br> <br>

        <label for="">Alamat</label> <br>
        <textarea name="alamat" id="" cols="30" rows="10"></textarea> <br> <br>

        <p>Gender :</p>

        <input type="radio" id="html" name="gender" value="man">
        <label for="html">Man</label><br>
        <input type="radio" id="css" name="gender" value="man">
        <label for="css">Woman</label><br>
        <input type="radio" id="javascript" name="gender" value="other">
        <label for="javascript">other</label> <br> <br>

        <label for="">Kebangsaan :</label>
        <select id="" name="kebangsaan">
            <option value="Indonesia">Indonesia</option>
            <option value="Inggris">Inggris</option>
            <option value="USA">USA</option>
            <option value="Arab Saudi">Arab Saudi</option>
        </select> <br> <br>

        <label for="">Bahasa :</label> <br>
        <input type="checkbox" id="" name="bahasa" value="indonesia">
        <label for="vehicle1"> Indonesia</label><br>
        <input type="checkbox" id="" name="bahasa" value="inggris">
        <label for="vehicle2"> Inggris</label><br>
        <input type="checkbox" id="" name="bahasa" value="arab">
        <label for="vehicle2"> Arab</label><br> <br>
        
        <label for="">Bio :</label> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="kirim">
    </form>

    @endsection