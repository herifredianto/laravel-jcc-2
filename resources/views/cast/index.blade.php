@extends('layout.master')
@section('judul')
    <h1>Halaman Tampilan data</h1> <br>
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary">Create</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $casts)
          <tr>
              <td>{{ $key + 1}}</td>
              <td>{{ $casts->nama }}</td>
              <td>{{ $casts->umur }}</td>
              <td>{{ $casts->bio }}</td>
              <td>
                  <a href="/cast/{{$casts->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/cast/{{$casts->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <form action="/cast/{{$casts->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                  </form>
              </td>
          </tr>
      @empty
          
      @endforelse
    </tbody>
  </table>

@endsection