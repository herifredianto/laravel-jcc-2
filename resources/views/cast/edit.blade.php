@extends('layout.master')
@section('judul')
    <h1>Halaman Edit Register</h1> <br>
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="exampleInputEmail1">Nama :</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>            
        @enderror
        <div class="form-group">
            <label for="exampleInputPassword1">Umur :</label>
            <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>            
        @enderror
        <div class="form-group">
            <label for="exampleInputPassword1">Bio :</label>
            <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" placeholder="Bio">
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>            
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>    
@endsection