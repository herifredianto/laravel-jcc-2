@extends('layout.master')
@section('judul')
    <h1>Halaman Register</h1> <br>
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Nama :</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>            
        @enderror
        <div class="form-group">
            <label for="exampleInputPassword1">Umur :</label>
            <input type="text" class="form-control" name="umur" placeholder="Umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>            
        @enderror
        <div class="form-group">
            <label for="exampleInputPassword1">Bio :</label>
            <input type="text" class="form-control" name="bio" placeholder="Bio">
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>            
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>    
@endsection